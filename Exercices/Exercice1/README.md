# Cours de sémantique

## Premier pas en programmation logique

### Introduction

Cette première séance a deux objectifs:
- Vérifier que tous les outils fonctionnent bien sur votre machine.
- Tester le langage Prolog et commencer à vous familiariser.

La programmation logique étant un paradigme nouveau, il vous faudra un petit temps d'adaptation.
Prenez bien le temps de bien comprendre les exemples que nous présenterons.

### Première partie: Installation

La **première étape** est de lire en complet le README à la racine et de suivre toutes les instructions d'installations qui vous sont données.

**ATTENTION**: Veillez à ce que votre dépôt soit privé !


### Faire fonctionner SWI-Prolog

**SWI-Prolog** est simplement un interprète qui servira à exécuter votre code.
Vous ne programmerez pas dans la console de l'interprète. Il est donc important d'avoir un éditeur de code (e.g.: ***Atom***).
Sachez que la plupart de ces éditeurs fournissent des packages qui donne accès à de la coloration syntaxique, très pratique pour vous aider à coder.
`Atom -> Preferences -> Install` et vous écrivez `Prolog` dans la barre de recherche, puis vous ajoutez le package `language-prolog`.

Vous allez créer un fichier prolog `test.pl` à un emplacement de votre choix. Cependant vous devrez savoir où celui se trouve !
Copiez le code ci-dessous et collez le dans votre fichier Prolog.

```Prolog
% enfant(nomEnfant, nomParent)
enfant(alice, jean).
enfant(alice, luc).
enfant(luc, julie).
enfant(julie, marnie).

grandParent(PetitEnfant, GrandParent) :-
   enfant(PetitEnfant, Parent),
   enfant(Parent, GrandParent).

% Exemple: grandParent(alice, julie).

% Cas de base
descendant(P, P).

% Cas récursif (Est-ce que X est un descendant de Y ?)
descendant(X, Y) :-
  enfant(X, Z),
  descendant(Z, Y).

% Exemple: descendant(alice, X).
```

Ouvrez désormais l'interprète **SWI-Prolog**.
Il faut lui dire le dossier où se trouve notre fichier:
`working_directory(_, '/Dossier/De/Mon/Fichier/Prolog').`
Si vous utilisez un terminal Unix, vous pouvez vous servir de la commande `pwd` à l'endroit où vous avez créé le fichier pour obtenir le chemin complet à copier/coller.

Il nous faut indiquer désormais quel fichier importer:
`[test].`
ou
`consult('test.pl').`

Au moment d'importer le fichier, l'interprète Prolog vous dira s'il y a une erreur.

Testons maintenant le code que nous avons copier/coller plus tôt dans le fichier `test.pl`.

Dans votre terminal Prolog, exécutez les commandes suivantes une à une et expliquez ce que vous obtenez:

- Exemple 1: `grandParent(alice, julie).`

- Exemple 2: `grandParent(alice, panda).`

- Exemple 3: `grandParent(alice, X).`

- Exemple 4: `descendant(alice, panda).`

- Exemple 5: `descendant(alice, jean).`

- Exemple 6: `descendant(alice, julie).`

- Exemple 7: `descendant(alice, marnie).`

- Exemple 8: `descendant(alice, R).`
Que se passe-t-il si vous appuyez sur la ***barre espace*** ou si vous appuyez sur la touche ***enter*** ?

La barre espace permet d'afficher toutes les solutions, tandis que la touche enter vous retournera le premier résultat vrai.

### Comprendre le principe d'unification

#### Explication et exemple

Quand vous appelez une règle dans votre terminal, Prolog va chercher à appliquer la règle qui *match* dans votre base de connaissance (Il est possible d'avoir plusieurs règles qui s'appliquent, auxquelles cas elles s'appliquent toutes.

Prenons un exemple:

Sur le terminal Prolog, nous rentrons la commande:
`grandParent(alice, julie).`
Par rapport à notre base de connaissances, nous avons une seule règle `grandParent`.
Prolog va donc prendre cette règle et faire correspondre les valeurs données à l'appel avec la règle de base.

Pour rappel, nous avons la règle suivante:
```Prolog
grandParent(PetitEnfant, GrandParent) :-
   enfant(PetitEnfant, Parent),
   enfant(Parent, GrandParent).
```

Prolog fait la correspondance entre la signature:
`grandParent(alice, julie)` et `grandParent(PetitEnfant, GrandParent)`, pour en déduire que:
`PetitEnfant = alice` et `GrandParent = julie`.
On obtient donc:
`grandParent(alice, julie) :- enfant(alice, Parent), enfant(Parent, julie).`

Désormais, Prolog doit encore résoudre `Parent`, trouver une valeur à notre variable qui satisfera à la fois `enfant(alice, Parent)` et `enfant(Parent, julie)`.
Il n'y a pas de magie, Prolog va tester toutes les valeurs possibles pour `Parent`, jusqu'à en trouver une (ou plusieurs) qui respecte ces deux relations.
Quand nous le ferons à la main, nous ne prendrons pas la peine de tester les cas que nous savons faux, mais gardez bien à l'esprit que Prolog le fait, vu qu'il ne le sait pas à l'avance !

En ce sens, nous parlons d'unification vu que nous prenons les informations données au départ et les mettons en relation (les unifions) avec les règles applicables.

#### À vous de jouer

En partant de l'exemple au-dessus, reprenez les exemples précédents et exécutez les à la main pour tenter de retrouver le(s) résultat(s) précédents.

Prenez le temps de le faire, toute la compréhension de la programmation logique résulte dans cet aspect !

***Pour vous aider***: Vous pouvez utiliser le debugger de **SWI-Prolog** pour voir toutes vos traces d'exécutions.
Vous verrez ainsi toutes les étapes d'exécutions et d'unifications.
Il vous suffit de rentrer dans le terminal la commande:
`trace.`
Pour désactiver le mode debug entrez la commande: `nodebug.`

### Les naturels et les listes !

#### Les naturels

Dans le dossier `/exercice1` se trouve un fichier `exercice1.pl`.
Importez le de la même manière que vous avez fait pour le fichier `test.pl`.

Dans cet exemple, nous allons manipuler notre propre type entier à l'aide de la suite de **Peano**.
Dans un second temps nous regarderons comment écrire des listes en Prolog.

En vous aidant du code Prolog, expliquez à la main étape par étape comment fonctionne l'appel des règles suivantes:
Attention, nous manipulons notre propre type entier et non celui de Prolog, donc `0 ≠ zero`.

- Exemple 1: `nat(s(s(zero))).`
- Exemple 2: `add(zero, s(zero), s(zero)).`
- Exemple 3: `add(zero, s(zero), R).`
- Exemple 4: `add(s(zero), s(zero), R).`
- Exemple 5: `gt(s(zero), zero).`
- Exemple 6: `gt(s(zero), s(zero)).`
- Exemple 7: `gt(s(s(zero)), s(zero)).`

#### Les listes

Une liste en Prolog s'écrit entre `[]`.
Ex: `['a', 'b', 'c']` une liste contenant des caractères.
Prolog donne accès à un mécanisme pour parcourir et décomposer les listes.
La liste `['a', 'b', 'c']` peut être vu comme un premier élément (qui n'est pas une liste) et le reste d'une liste (qui est une liste):
`['a' | 'b', 'c']` (Premier élément -> `a`, le reste de la liste -> `['b', 'c']` )

Prenons l'exemple de la fonctions contains dans le code:

```Prolog
contains(X, [X|_]).
contains(X, [Y|T]) :- not(X = Y), contains(X, T).
```

Nous avons deux cas:
- L'élément `X` est le premier élément de la liste parcourue. `_` symbolise que nous ne nous soucions pas de ce qu'il y a dans le reste de la liste.
- Nous voulons savoir si l'élément `X` est contenu dans une liste `[Y|T]`. Son premier élément est `Y`, et le reste de la liste `T`. Nous vérifions bien que `X` est différent de `Y`, puis nous appelons de nouveau `contains` sur le reste de la liste.

En vous aidant du code Prolog, expliquez à la main étape par étape comment fonctionne l'appel des règles suivantes:

- Exemple 1: `contains(2, [1,2]).`
- Exemple 2: `contains(2, [1,3]).`
- Exemple 3: `max([1,2], 2).`
- Exemple 4: `max([1,2], 1).`
- Exemple 5: `max([1,2], R).`



### Vos premières règles !

Les exemples précédents n'ont déjà plus de secrets pour vous ?

À vous de jouer et d'implémenter les exemples suivants:
- La taille d'une liste
- La somme d'une liste

Attention: Si vous désirez faire une opération avec les naturels et l'affecter à une variable, il vous faudra utiliser le mot-clé `is` et non `=`.
De plus, il faut faire attention à l'ordre des éléments de vos règles.
Vu que nous avons un principe de déduction, il faut parfois obtenir certaines informations avant de pouvoir en deviner d'autres.
