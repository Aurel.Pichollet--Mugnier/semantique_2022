% Quand vous aurez réussi à importer ce fichier Prolog, toutes les règles
% seront prises en compte dans votre terminal Prolog.
% Pour chaque règle il y a un exemple que vous pouvez directement
% copier/coller dans votre terminal Prolog pour tester.

% -----------------------------------------------------------------%

% Construction des naturels.
% Fait de base.
nat(zero).
% Règle récursive. Si la tête n'est pas zero, on vérifie que la tête est "s"
% et on rappelle la règle sur le reste.
nat(s(X)) :- nat(X).

% Exemple:
% nat(zero). -> true
% nat(s(s(zero))). -> true

% L'addition
add(zero, Y, Y).
add(s(X) , Y, Res) :- add(X, s(Y), Res).

% Exemple:
% add(s(zero), s(zero), Res). retourne Res = s(s(zero))

% Greater than: X > Y
gt(s(_), zero).
gt(s(X), s(Y)) :- gt(X,Y).

% Exemple:
% gt(s(s(zero)), s(zero)). -> true


% -----------------------------------------------------------------%

% Manipulation des listes en Prolog

% Est-ce que ma liste contient un élément particulier ?
contains(X, [X|_]).
contains(X, [Y|T]) :- not(X = Y), contains(X, T).

% Exemple:
% contains(5, [6,4,3,5,1]).

max([H], H).
max([H1|T], H1) :- max(T, Res), H1 >= Res.
max([H1|T], Res) :- max(T, Res), H1 < Res.

% Exemple:
% max([1,4,42,7,20], R). -> R = 42
